#!/bin/bash

/docker-entrypoint.sh "$@"

while [ ! -d /etc/letsencrypt/live/${CERT_NAME} ]; do
	echo "Waiting for certificates";
	sleep 10s & wait ${!};
done

mv /etc/nginx/conf.d/vhost_https.conf.disabled /etc/nginx/conf.d/vhost_https.conf
nginx -s reload;

while :; do
	sleep 6h & wait ${!};
	echo "Reloading"
	nginx -s reload;
done
