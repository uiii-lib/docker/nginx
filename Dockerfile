FROM nginx

RUN rm /etc/nginx/conf.d/default.conf;

COPY templates /etc/nginx/templates
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]

CMD ["nginx"]
